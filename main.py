#!/usr/bin/env python3

from PySide import QtCore, QtGui, QtUiTools, QtNetwork
from const import LANGUAGES_FROM, LANGUAGES_TO
from widgets import LoadingSpinner, TicksSpinner
from translate import TranslateService
from operator import itemgetter
import lxml.html
import logging
import resrc


class MainWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        loader = QtUiTools.QUiLoader()
        file = QtCore.QFile(":/main.ui")
        file.open(QtCore.QFile.ReadOnly)
        self.ui = loader.load(file, self)
        file.close()

        layout = QtGui.QVBoxLayout()
        layout.addWidget(self.ui)
        layout.setContentsMargins(4, 4, 4, 4)
        self.setLayout(layout)
        self.setWindowTitle("Copytrans")
        self.setWindowIcon(QtGui.QIcon(":/img/icon.png"))

        self.ui.o_text.setAcceptRichText(False)

        # vars
        self.latest_data = self.getData()
        self.need_update = True
        self.lng_source_current = 'en'
        self.lng_target_current = 'fr'
        self.lng_source_list = []
        self.lng_target_list = []

        # misc obj
        self.mgr = QtNetwork.QNetworkAccessManager()
        self.settings = QtCore.QSettings(QtCore.QSettings.IniFormat, QtCore.QSettings.UserScope, "ZopiSoftware",
                                         "Copytrans", self)
        self.clipboard = QtGui.QApplication.clipboard()
        self.translate = TranslateService(self)

        self.load_spinner = LoadingSpinner(TicksSpinner(size=64, color=QtCore.Qt.black, ticks=21, innerRadius=18), 10,
                                           .7, self.ui.t_text)

        self.update_timer = QtCore.QTimer()
        self.update_timer.setInterval(1500)
        self.update_timer.setSingleShot(False)

        # signals
        self.update_timer.timeout.connect(self.updateNow)
        self.ui.lng_from.currentIndexChanged.connect(self.needUpdate)
        self.ui.lng_to.currentIndexChanged.connect(self.needUpdate)
        self.ui.o_text.textChanged.connect(self.needUpdate)
        self.ui.lng_switch.clicked.connect(self.switchLanguages)

        self.ui.o_clear.clicked.connect(self.ui.o_text.clear)
        self.ui.o_copy.clicked.connect(self.copyOriginal)
        self.ui.o_paste.clicked.connect(self.pasteOriginal)
        self.ui.t_copy.clicked.connect(self.copyTranslated)

        self.ui.font_size.valueChanged.connect(self.updateFontSize)

        # inits
        # self.setupLanguageSelect()
        self.readSettings()
        self.update_timer.start()
        self.translate.csi_load()  # hello to gstatic

    def refreshLanguages(self):
        logging.info("Refreshing languages")
        request = QtNetwork.QNetworkRequest(QtCore.QUrl("https://translate.google.com/"))
        request.setRawHeader("User-Agent", "Mozilla/5.0 (compatible)")
        reply = self.mgr.get(request)

        def parse_languages():
            soup = lxml.html.fromstring(str(reply.readAll()))

            def get_options(name):
                return [(l.get('value'), l.text)
                        for l in soup.xpath('//select[@name="%s"]' % name)[0].findall('option')
                        if l.get('value') not in ('auto', 'separator')]

            self.lng_source_list = get_options('sl')
            self.lng_target_list = get_options('tl')
            logging.info("Retrieved %d source and %d target languages",
                         len(self.lng_source_list), len(self.lng_target_list))
            self.setupLanguageSelect()
            self.writeSettings()

        reply.finished.connect(parse_languages)

    def readSettings(self):
        self.settings.beginGroup('main')
        self.resize(self.settings.value('size', QtCore.QSize(400, 400)))
        self.move(self.settings.value('pos', QtCore.QPoint(200, 200)))
        self.ui.font_size.setValue(int(self.settings.value('font_size', self.ui.o_text.font().pointSize())))
        self.lng_source_list = self.settings.value('lng/source/list')
        self.lng_target_list = self.settings.value('lng/target/list')
        self.lng_source_current = self.settings.value('lng/source/current')
        self.lng_target_current = self.settings.value('lng/target/current')
        self.settings.endGroup()
        if not self.lng_source_list or not self.lng_target_list:
            self.refreshLanguages()
        else:
            self.setupLanguageSelect()

    def writeSettings(self):
        lng_from, lng_to, _ = self.getData()
        print("SETTINGS?", lng_from, lng_to)
        self.settings.beginGroup('main')
        self.settings.setValue('lng/source/current', lng_from)
        self.settings.setValue('lng/target/current', lng_to)
        self.settings.setValue('lng/source/list', self.lng_source_list)
        self.settings.setValue('lng/target/list', self.lng_target_list)
        self.settings.setValue('font_size', self.ui.font_size.value())
        self.settings.setValue('size', self.size())
        self.settings.setValue('pos', self.pos())
        self.settings.endGroup()

    def closeEvent(self, e):
        self.writeSettings()
        super().closeEvent(e)

    def selectItemByData(self, input, data):
        input.setCurrentIndex(input.findData(data))

    def setupLanguageSelect(self):
        def fill(editor, items):
            editor.clear()
            for code, name in items:
                editor.addItem(name, userData=code)

        fill(self.ui.lng_from, self.lng_source_list)
        fill(self.ui.lng_to, self.lng_target_list)
        self.selectItemByData(self.ui.lng_from, self.lng_source_current)
        self.selectItemByData(self.ui.lng_to, self.lng_target_current)

    def getData(self):
        self.lng_source_current = self.ui.lng_from.itemData(self.ui.lng_from.currentIndex())
        self.lng_target_current = self.ui.lng_to.itemData(self.ui.lng_to.currentIndex())
        original = self.ui.o_text.toPlainText().strip()
        return self.lng_source_current, self.lng_target_current, original

    def copyOriginal(self):
        text = self.ui.o_text.toPlainText().strip()
        if text:
            self.clipboard.setText(text)

    def copyTranslated(self):
        text = self.ui.t_text.toPlainText().strip()
        if text:
            self.clipboard.setText(text)

    def pasteOriginal(self):
        content = self.clipboard.text().strip()
        if content:
            self.ui.o_text.setPlainText(content)

    def switchLanguages(self):
        lfrom, lto, _ = self.getData()
        self.selectItemByData(self.ui.lng_from, lto)
        self.selectItemByData(self.ui.lng_to, lfrom)
        self.needUpdate()

    def needUpdate(self, *args):
        self.need_update = True
        self.load_spinner.fadeIn()

    def updateNow(self):
        if not self.need_update:
            self.load_spinner.fadeOut()
            return
        self.need_update = False

        new_data = self.getData()
        if new_data == self.latest_data:
            self.load_spinner.fadeOut()
            return
        lng_from, lng_to, original = new_data
        if not original:  # original is empty, clean result, exit
            self.ui.t_text.clear()
            self.load_spinner.fadeOut()
            return

        logging.info("Translating from %s to %s", lng_from, lng_to)
        self.translate.get_translation(lng_from, lng_to, original, self.translationResult)

    def translationResult(self, data, reply, _):
        logging.debug("Translation arrived")
        translated = []

        if data[4] is None:  # from POST... no other suggestions
            translated = [_[0] for _ in data[0]]
        else:
            was_n_before = True
            for piece in data[4]:
                if piece[2] == 1 and not was_n_before:
                    translated.append(" ")
                translated.append(piece[0])
                was_n_before = piece[0].endswith("\n")

        self.ui.t_text.setPlainText("".join(translated).strip())
        self.load_spinner.fadeOut()

    def updateFontSize(self, value):
        for input in (self.ui.o_text, self.ui.t_text):
            f = input.font()
            f.setPointSize(value)
            input.setFont(f)


def main():
    import sys

    logging.basicConfig(level=logging.INFO, format='[%(name)s] %(asctime)s %(levelname)s -- %(message)s')

    app = QtGui.QApplication(sys.argv)
    file = QtCore.QFile("res/style.css")
    file.open(QtCore.QFile.ReadOnly)
    app.setStyleSheet(str(file.readAll()))
    file.close()
    window = MainWidget()
    window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()

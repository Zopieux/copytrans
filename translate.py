from PySide import QtCore, QtNetwork
import json
from collections import deque, defaultdict
from functools import partial
import time
import random
from utils import parse_eval
from const import MAX_LENGTH_FOR_GET

import logging

log = logging.getLogger('copytrans')

void = lambda *_: _

TRANSLATE_HOST = 'translate.google.com'
USER_AGENT = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36'
MAX_DELAY = 3000
REQUEST_DELAY = defaultdict(lambda: 500)
REQUEST_DELAY[(TRANSLATE_HOST, 80)] = 200
REQUEST_DELAY[('csi.gstatic.com', 80)] = 300


def build_csi(d):
    d.update({'v': '3', 's': 'translate', 'tran': '16', 'e': '17259,gbar2'})
    url = QtCore.QUrl('/csi')
    url.setQueryItems(list(d.items()))
    return {'host': 'csi.gstatic.com',
            'port': 80,
            'important': False,
            'priority': False,
            'path': url.toString(),
            'handler': void}


class TranslateService(QtCore.QObject):
    def __init__(self, parent=None):
        super(TranslateService, self).__init__(parent)
        self.manager = QtNetwork.QNetworkAccessManager()
        self.manager.finished.connect(self._process_reply)
        self.manager.authenticationRequired.connect(self._site_authenticate)
        self.manager.proxyAuthenticationRequired.connect(self._proxy_authenticate)
        self._last_request_times = {}
        self._active_requests = {}
        self._hosts = []
        self._high_priority_queues = {}
        self._low_priority_queues = {}
        self._timer = QtCore.QTimer(self)
        self._timer.setSingleShot(True)
        self._timer.timeout.connect(self._run_next_task)
        self._request_methods = {
            "GET": self.manager.get,
            "POST": self.manager.post,
            "PUT": self.manager.put,
            "DELETE": self.manager.deleteResource,
        }

    def _start_request(self, method, host, port, path, data, handler, is_json, http_auth=None, morehandlers=None):
        log.debug("%s http://%s:%d%s", method, host, port, path)
        url = QtCore.QUrl.fromEncoded("http://%s:%d%s" % (host, port, path))
        if http_auth is not None:
            url.setUserName(http_auth[0])
            url.setPassword(http_auth[1])
        request = QtNetwork.QNetworkRequest(url)
        request.setRawHeader("User-Agent", USER_AGENT)
        request.setAttribute(QtNetwork.QNetworkRequest.CacheLoadControlAttribute, QtNetwork.QNetworkRequest.PreferCache)
        if data is not None:
            # we shall have a posting_json param cause query can be www-form and response can be json
            # request.setHeader(QtNetwork.QNetworkRequest.ContentTypeHeader, "application/json" if is_json else "application/x-www-form-urlencoded")
            request.setHeader(QtNetwork.QNetworkRequest.ContentTypeHeader, "application/x-www-form-urlencoded")
        send = self._request_methods[method]
        reply = send(request, data) if data is not None else send(request)
        key = (host, port)
        self._last_request_times[key] = time.time()
        self._active_requests[reply] = (request, handler, is_json)
        if morehandlers is not None:
            for handName, handHand in morehandlers:
                getattr(reply, handName).connect(partial(handHand, reply))
        return True

    @staticmethod
    def urls_equivalent(left_url, right_url):
        return (left_url.port(80) == right_url.port(80) and
                left_url.toString(QtCore.QUrl.FormattingOption(QtCore.QUrl.RemovePort)) ==
                right_url.toString(QtCore.QUrl.FormattingOption(QtCore.QUrl.RemovePort)))

    @staticmethod
    def encode_data(datadict):
        out = []
        for key, value in datadict.items():
            value = str(QtCore.QUrl.toPercentEncoding(value))
            out.append('%s=%s' % (key, value))
        return '&'.join(out)

    def _process_reply(self, reply):
        try:
            request, handler, is_json = self._active_requests.pop(reply)
        except KeyError:
            logging.error("Error: Request not found for %s" % str(reply.request().url().toString()))
            return
        error = int(reply.error())
        redirect = reply.attribute(QtNetwork.QNetworkRequest.RedirectionTargetAttribute)
        logging.debug("Received reply for %s: HTTP %d (%s)",
                      reply.request().url().toString(),
                      reply.attribute(QtNetwork.QNetworkRequest.HttpStatusCodeAttribute),
                      reply.attribute(QtNetwork.QNetworkRequest.HttpReasonPhraseAttribute))
        if handler is not None:
            if error:
                log.error("Network request error for %s: %s (QT code %d, HTTP code %d)",
                          reply.request().url().toString(),
                          reply.errorString(),
                          error,
                          reply.attribute(QtNetwork.QNetworkRequest.HttpStatusCodeAttribute))

            # Redirect if found and not infinite
            if redirect is not None and not TranslateService.urls_equivalent(redirect, reply.request().url()):
                log.debug("Redirect to %s requested", redirect.toString())
                self.get(
                    str(redirect.host()),
                    redirect.port(80),
                    # retain path, query string and anchors from redirect URL
                    redirect.toString(
                        QtCore.QUrl.FormattingOption(QtCore.QUrl.RemoveAuthority | QtCore.QUrl.RemoveScheme)),
                    handler,
                    is_json
                )
            elif is_json:
                data = str(reply.readAll())
                try:
                    parsed = json.loads(data)
                except ValueError:
                    parsed = parse_eval(data)
                # print(parsed)
                handler(parsed, reply, error)
            else:
                handler(reply.readAll(), reply, error)
        reply.close()

    def get(self, host, port, path, handler, is_json=False, priority=False, important=False, http_auth=None,
            morehandlers=None):
        func = partial(self._start_request, "GET", host, port, path, None, handler, is_json, http_auth, morehandlers)
        return self.add_task(func, host, port, priority, important=important)

    def post(self, host, port, path, data, handler, is_json=False, priority=True, important=True, http_auth=None,
             morehandlers=None):
        logging.debug("POST-DATA %r", data)
        func = partial(self._start_request, "POST", host, port, path, data, handler, is_json, http_auth, morehandlers)
        return self.add_task(func, host, port, priority, important=important)

    def put(self, host, port, path, data, handler, priority=True, important=True, http_auth=None, morehandlers=None):
        func = partial(self._start_request, "PUT", host, port, path, data, handler, False, http_auth, morehandlers)
        return self.add_task(func, host, port, priority, important=important)

    def delete(self, host, port, path, handler, priority=True, important=True, http_auth=None, morehandlers=None):
        func = partial(self._start_request, "DELETE", host, port, path, None, handler, False, http_auth, morehandlers)
        return self.add_task(func, host, port, priority, important=important)

    def _site_authenticate(self, reply, authenticator):
        self.emit(QtCore.SIGNAL('authentication_required'), reply, authenticator)

    def _proxy_authenticate(self, proxy, authenticator):
        self.emit(QtCore.SIGNAL('proxyAuthentication_required'), proxy, authenticator)

    def stop(self):
        self._high_priority_queues = {}
        self._low_priority_queues = {}
        for reply in self._active_requests.keys():
            reply.abort()

    def _run_next_task(self):
        delay = 999999
        for key in self._hosts:
            queue = self._high_priority_queues.get(key) or self._low_priority_queues.get(key)
            if not queue:
                continue
            now = time.time()
            last = self._last_request_times.get(key)
            request_delay = REQUEST_DELAY[key]
            last_ms = (now - last) * 1000 if last is not None else request_delay
            if last_ms >= request_delay:
                log.debug("Last request to %s was %d ms ago, starting another one", key, last_ms)
                d = request_delay
                queue.popleft()()
            else:
                d = request_delay - last_ms
                log.debug("Waiting %d ms before starting another request to %s", d, key)
            if d < delay:
                delay = d
        if delay < MAX_DELAY:
            self._timer.start(delay)

    def add_task(self, func, host, port, priority, important=False):
        key = (host, port)
        if key not in self._hosts:
            self._hosts.append(key)
        if priority:
            queues = self._high_priority_queues
        else:
            queues = self._low_priority_queues
        queues.setdefault(key, deque())
        if important:
            queues[key].appendleft(func)
        else:
            queues[key].append(func)
        if not self._timer.isActive():
            self._timer.start(0)
        return key, func, priority

    def remove_task(self, task):
        key, func, priority = task
        if priority:
            queue = self._high_priority_queues[key]
        else:
            queue = self._low_priority_queues[key]
        try:
            queue.remove(func)
        except ValueError:
            pass

    # real methods
    def get_translation(self, lng_from, lng_to, original, callback):
        def build_get_args(args):
            output = []
            for key, value in args.items():
                if isinstance(value, str):
                    output.append((key, value))
                else:
                    output.extend((key, v) for v in value)
            return output

        meta_data = {
            'client': 't',
            'hl': 'fr',
            'multires': '1',
            'kc': '1',
            'tk': '521198|576167',
            'ssel': '0',
            'tsel': '0',
            'dt': ['bd', 'ex', 'ld', 'md', 'qca', 'rw', 'rm', 'ss', 't', 'at'],
            'sl': lng_from,
            'tl': lng_to,
            'ie': 'UTF-8',
            'oe': 'UTF-8',
        }
        data = {'q': original}
        qs = QtCore.QUrl('/translate_a/single')
        self.csi_translation(lng_from, lng_to, len(original))

        if len(original) <= MAX_LENGTH_FOR_GET:
            meta_data.update(data)
            meta_data['otf'] = '1' if random.random() < 0.91 else '2'  # automatic translation (key down)
            qs.setQueryItems(build_get_args(meta_data))
            return self.get(TRANSLATE_HOST, 80, qs.toString(), is_json=True, priority=True, important=True,
                            handler=callback)

        else:
            meta_data['source'] = 'btn'
            qs.setQueryItems(build_get_args(meta_data))
            data = TranslateService.encode_data(data)
            return self.post(TRANSLATE_HOST, 80, qs.toString(), data, is_json=True, priority=True, important=True,
                             handler=callback)

    def csi_load(self):
        return self.get(**build_csi({
            'rt': 'cl.65,rsw.114,rsl.114,rtl.114,jbl.150,jl.175,br.178,prt.186,ol.242',
            'srt': str(random.randint(100, 200)), 'action': 't'}))

    def csi_translation(self, lng_from, lng_to, length):
        ri = random.randint
        return self.get(**build_csi({
            'rt': 'prt.%d,ol.%d' % (ri(10, 100), ri(10, 100)), 'it': 'st.%d' % ri(10, 210), 'action': 'at',
            'sl': lng_from, 'tl': lng_to, 'size': str(length)}))

from PySide.QtCore import *
from PySide.QtGui import *
import math

CORNER_ROUNDNESS = 6
FADING_DURATION = 300
SPINNER_FADING_DURATION = 200
FONT_SIZE = 11
OPACITY = 0.7


class LoadingSpinner(QWidget):
    def __init__(self, spinner, bg_padding=8, bg_opacity=.1, parent=None):
        super().__init__(parent)
        self.m_bg_padding = bg_padding
        self.m_bg_opacity = bg_opacity
        self.m_spinner = spinner
        self.resize(self.m_spinner.size() + bg_padding, self.m_spinner.size() + bg_padding)

        self.m_showHide = QTimeLine()
        self.m_showHide.setDuration(SPINNER_FADING_DURATION)
        self.m_showHide.setStartFrame(0)
        self.m_showHide.setEndFrame(300)
        self.m_showHide.setUpdateInterval(20)

        self.m_spinner.frameChanged.connect(self.update)

        self.m_showHide.frameChanged.connect(self.update)
        self.m_showHide.finished.connect(self.hideFinished)

        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.hide()

    def fadeIn(self):
        if self.isVisible():
            return

        self.show()

        self.m_spinner.start()
        self.m_showHide.setDirection(QTimeLine.Forward)

        if self.m_showHide.state() != QTimeLine.Running:
            self.m_showHide.start()

    def fadeOut(self):
        self.m_showHide.setDirection(QTimeLine.Backward)

        if self.m_showHide.state() != QTimeLine.Running:
            self.m_showHide.start()

    def hideFinished(self):
        if self.m_showHide.direction() == QTimeLine.Backward:
            self.hide()
            self.m_spinner.stop()

    def paintEvent(self, ev):
        if not self.parentWidget():
            return

        center = QPoint((self.parentWidget().width() / 2) - (self.width() / 2),
                        (self.parentWidget().height() / 2) - (self.height() / 2))
        if center != self.pos():
            self.move(center)
            return

        p = QPainter(self)
        r = QRect(self.contentsRect())

        if self.m_showHide.state() == QTimeLine.Running:
            p.setOpacity(self.m_showHide.currentValue())

        r_, g_, b_, a_ = QColor(self.m_spinner.m_color).getRgbF()
        p.setRenderHint(QPainter.Antialiasing)
        p.setPen(Qt.NoPen)
        p.setBrush(QColor.fromRgbF(1 - r_, 1 - g_, 1 - b_, self.m_bg_opacity))
        p.drawRoundedRect(r, 6, 6)
        pixmap = self.m_spinner.getFrame()
        p.drawPixmap(QRect(self.m_bg_padding / 2, self.m_bg_padding / 2, self.m_spinner.size(), self.m_spinner.size()),
                     pixmap)


class SpinnerPainter(QObject):
    frameChanged = Signal()

    def __init__(self, size=64, color=Qt.black, innerRadius=18, duration=1000, speed=300, tailLength=1, clockWise=True):
        # super().__init__(self) # wtf? not working
        QObject.__init__(self)

        self.m_size = size
        self.m_innerRadius = innerRadius
        self.m_speed = speed
        self.m_duration = duration
        self.m_tailLength = tailLength
        self.m_color = color
        self.m_clockWise = clockWise

        self.m_anim = QTimeLine()
        self.m_anim.setDuration(self.m_duration)
        self.m_anim.setLoopCount(0)

        self.m_anim.frameChanged.connect(lambda: self.frameChanged.emit())

        self.m_key = self.hashSoup(self.__class__.__name__, size, innerRadius, color, duration, speed, tailLength,
                                   clockWise)

    def hashSoup(self, *params):
        return '/'.join(map(str, params))

    def rad2deg(self, rad):
        return rad * 180 / math.pi

    def start(self):
        self.m_anim.start()

    def stop(self):
        self.m_anim.stop()

    def running(self):
        return self.m_anim.state() == QTimeLine.Running

    def size(self):
        return self.m_size

    def paintFrame(self, pixmap, framenum):
        raise NotImplementedError

    def getFrame(self):
        pixmap = QPixmap()
        if self.m_clockWise:
            framenum = self.m_anim.currentFrame()
        else:
            framenum = self.m_anim.endFrame() - self.m_anim.currentFrame()

        key = self.hashSoup(self.m_key, framenum)
        if not QPixmapCache.find(key, pixmap):
            pixmap = QPixmap(self.size(), self.size())
            pixmap.fill(Qt.transparent)
            self.paintFrame(pixmap, framenum)
            QPixmapCache.insert(key, pixmap)

        return pixmap


class TicksSpinner(SpinnerPainter):
    def __init__(self, ticks=16, tickWidth=2, **kwargs):
        super().__init__(**kwargs)

        self.m_outerRadius = self.size() / 2
        self.m_tickWidth = tickWidth
        self.m_ticks = ticks

        self.m_key = self.hashSoup(self.m_key, ticks, tickWidth)

        self._anglediff = 2 * math.pi / self.m_ticks
        trans = QColor(self.m_color)
        trans.setAlphaF(0)
        self._grad = QConicalGradient()
        self._grad.setStops((
            (0, self.m_color),
            (self.m_tailLength, trans),
        ))
        self._pen = QPen(self._grad, self.m_tickWidth, Qt.SolidLine, Qt.RoundCap)
        self.m_anim.setCurveShape(QTimeLine.LinearCurve)
        self.m_anim.setStartFrame(0)
        self.m_anim.setEndFrame(self.m_ticks)
        self.m_anim.setUpdateInterval(self.m_speed / self.m_ticks)

    def paintFrame(self, pixmap, frame):
        pp = QPainter(pixmap)
        pp.setRenderHint(QPainter.Antialiasing)
        pp.translate(self.m_outerRadius, self.m_outerRadius)

        a = ((self.m_ticks - frame) % self.m_ticks) * self._anglediff
        self._grad.setAngle(self.rad2deg(a + self._anglediff / 2))  # offset prevents discontinuity to be shown
        self._pen.setBrush(self._grad)
        pp.setPen(self._pen)

        for n in range(self.m_ticks):
            otr = self.m_outerRadius - self.m_tickWidth  # prevents painting out the pixmap
            innerPoint = QPointF(
                self.m_innerRadius * math.cos(n * self._anglediff),
                self.m_innerRadius * math.sin(n * self._anglediff)
            )
            outerPoint = QPointF(
                otr * math.cos(n * self._anglediff),
                otr * math.sin(n * self._anglediff)
            )
            pp.drawLine(innerPoint, outerPoint)  # draw the nth tick of this frame

        pp.end()


class RingSpinner(SpinnerPainter):
    def __init__(self, step=20, **kwargs):
        super().__init__(**kwargs)

        self.m_step = step
        self.m_outerRadius = self.size() / 2

        self.m_key = self.hashSoup(self.m_key, self.m_step)

        self._anglediff = 2 * math.pi / self.m_step
        trans = QColor(self.m_color)
        trans.setAlphaF(0)
        self._grad = QConicalGradient()
        self._grad.setStops((
            (0, self.m_color),
            (self.m_tailLength, trans),
        ))
        self._pen = QPen(self._grad, self.m_outerRadius - self.m_innerRadius, Qt.SolidLine, Qt.RoundCap)
        self._r = self.m_innerRadius + (self.m_outerRadius - self.m_innerRadius) / 2
        self.m_anim.setCurveShape(QTimeLine.LinearCurve)
        self.m_anim.setStartFrame(0)
        self.m_anim.setEndFrame(self.m_step)
        self.m_anim.setUpdateInterval(self.m_speed / self.m_step)

    def paintFrame(self, pixmap, frame):
        pp = QPainter(pixmap)
        pp.setRenderHint(QPainter.Antialiasing)
        pp.translate(self.m_outerRadius, self.m_outerRadius)

        a = ((self.m_step - frame) % self.m_step) * self._anglediff
        self._grad.setAngle(self.rad2deg(a))  # offset prevents discontinuity to be shown

        self._pen.setBrush(self._grad)
        pp.setPen(self._pen)
        pp.drawEllipse(QPoint(0, 0), self._r, self._r)

        pp.end()

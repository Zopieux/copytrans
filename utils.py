import json
import re


def parse_eval(s):
    return json.loads(re.sub(r'(?<=,)\s*,', ' null,', s))
